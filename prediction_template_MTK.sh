#!/bin/sh -evx 
#############################################################################
#
# INITIAL DEVELOPMENT: Madlen Kimmritz, Sep 2019
#
# This script is a further development of prediction_test.in
#          STEPs named with lower case (a,b,d,f): as in prediction_test.in
#          STEPS named with capital letters (C,E): extention for use of DYNLIB  
#
# PURPOSE: a) Create case with enabled use of dynlib for NorESM predictions 
#             to derive weather features and storm track diagnostics (DYNLIB)
#          b) Submit script to run prediction
#
# HOW TO USE:
# 1) adapt setting_MTK.sh
# 2) adapt prediction_test.in
# 3) execute this script
#
# see also README_MTK.txt and documentation_MTK.pdf
#
############################################################################

SCRIPTPATH=`readlink -f $0`
SCRIPTDIR=`dirname $SCRIPTPATH`
INFILE=$SCRIPTDIR/../prediction/use_cases/NorESM1-CMIP6_hist.in
WFFILE=$SCRIPTDIR/../MTK/setting_MTK.sh

cd $SCRIPTDIR/../prediction

# STEP a (inherited from NorESM)
echo OBTAIN RESTARTS FROM REMOTE LOCATION 
$SCRIPTDIR/../prediction/obtain_restarts.sh $INFILE 

# STEP b (inherited from NorESM)
echo CREATE TEMPLATE CASE  
$SCRIPTDIR/../prediction/create_template.sh $INFILE 

# STEP C (MIND the KAP specifics)
echo PREPARE SETTING AND TEMPLATE FOR USE OF DYNLIB
$SCRIPTDIR/../MTK/modify_setting_MTK.sh $INFILE $WFFILE

# STEP d (inherited from NorESM)
echo CREATE ENSEMBLE  
$SCRIPTDIR/../prediction/create_ensemble.sh $INFILE 

# STEP E (MIND the KAP specifics)
echo ADAPT SUBMISSION SCRIPT FOR DYNLIB APPLICATION
$SCRIPTDIR/../MTK/modify_submit_MTK.sh $INFILE $WFFILE

# STEP f (inherited from NorESM)
echo SUBMIT ENSEMBLE  
$SCRIPTDIR/../prediction/submit_ensemble.sh $INFILE  


