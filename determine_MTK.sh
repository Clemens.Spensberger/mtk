#!/bin/sh
#############################################################################
#
# INITIAL DEVELOPMENT: Madlen Kimmritz, Sep 2019
#
# PURPOSE: for the last STOP_N (..*STOP_OPTION) months: 
#          a) derive weather features
#          b) store data in archive
#          c) dump DYN_h1 cam output (if WF_RETAIN = 0) or delete
#
# This function - is called in the beginning of st_archive.sh - before data are moved
#
# Required input: setting file of case (e.g. prediction_test.in or analysis_setting.sh)
#                 settign file of MTK
#
# NOTE: - weather feature data are moved into archive/wfe folder 
#
############################################################################
start=$(date +'%s')
module load Python/3.6.4-intel-2018a  
module load ScaLAPACK/2.0.2-gompi-2018a-OpenBLAS-0.2.20
module load netcdf4-python/1.4.0-intel-2018a-Python-3.6.4

NAD_SCRIPT='cam_monthly_analyses.py'

source $2
if [ $WF_IS_PREDICTION == 1 ]; then
  . $1
  sta=${DOUT_S_ROOT}
  llen=${#CASE}
  sub=${CASE:0:len-6}
  CCASETOOLS=${CASESROOT}/$sub/$CASE/Tools/  
  ACTUAL_PATH1=`pwd`  #CASESROOT not given in analysis
                     # modify_submit_MK.sh for analysis case 
                     # inserts line to move to Tools folder
                     # right before calling determine_MTK.sh
 cd ${WORK}/noresm/$sub/$CASE/run
else
 source $1
 STOP_OPTION=nmonths
 STOP_N=1
 sta=${DOUT_S_ROOT}
 llen=${#CASE}
 sub=${CASE:0:len-6}
 ACTUAL_PATH1=`pwd`  #CASESROOT not given in analysis
                     # modify_submit_MK.sh for analysis case 
                     # inserts line to move to Tools folder
                     # right before calling determine_MTK.sh
 CCASETOOLS=${ACTUAL_PATH1}/
 cd ${WORK}/noresm/$sub/$CASE/run
fi

#=== sanity check and identification of months to apply feature detection to
if [ "${STOP_OPTION}" != "nmonths" ] && [ "${STOP_OPTION}" != "nyears" ]; then 
  echo "STOP_OPTION=" ${STOP_OPTION}
  echo "Invalid choice of STOP_OPTION (see "${1}"). Required: STOP_OPTION = nmonths or STOP_OPTION = nyears."
  echo "We quit."
  exit 1
fi
N_MONTHS=0
if [ "${STOP_OPTION}" == "nmonths" ]; then 
  N_MONTHS=${STOP_N}
elif [ "${STOP_OPTION}" == "nyears" ]; then 
  let N_MONTHS="12*STOP_N"
fi
#===== determine start and end month
set ${CASE}.cpl.r.*
cplfile=`ls -rt $* 2> /dev/null | tail -1`
dname=`echo $cplfile | sed "s/\.nc//; s/^.*\.r\.//;"`
dname1=${dname:0:10}
dname0=$(date -d "$dname1 - $N_MONTHS months" +"%Y-%m-%d")
DYEAR_START=${dname0:0:4}
DMONTH_START=${dname0:5:2}
#==== construct folder structure if not yet existent and move dynlib related data to there 
#     (as st_archive would move them to archive/.../atm/hist/.
#      and to avoid determining and setting links from the last 5 days of preceding month)

[[ -d ${DOUT_S_ROOT} ]] || mkdir  ${sta}
[[ -d ${DOUT_S_ROOT}"/wfe" ]] || mkdir  ${sta}"/wfe"
[[ -d ${DOUT_S_ROOT}"/wfe/dump" ]] || mkdir  ${sta}"/wfe/dump"

#==== get the data into the right location
let ACT_N1="DYN_H1"
mv ${CASE}.cam2.h${ACT_N1}.*nc ${sta}"/wfe/."
# move all possibly to archive/atm/hist/-moved cam2.h${ACT_N1} files to wfe
mv ${sta}/atm/hist/*cam2.h${ACT_N1}*nc ${sta}/wfe/.

#==== feature detection for the given time period || for casename:
dname00=$(date -d "$dname0 - 1 months" +"%Y-%m-%d")
dnameact=$dname00
#==== 
for mm in `seq -w 01 $N_MONTHS`
do 
  dnameact=$(date -d "$dnameact + 1 months" +"%Y-%m-%d")
  WF_YEAR=${dnameact:0:4}
  WF_MONTH=${dnameact:5:2}
  #==check existence of files 
  case ${WF_MONTH} in 
   01 | 03 | 05 | 07 | 08 | 10 | 12)
        REQ_NR=31 ;;   #4*31
   04 | 06 | 09 | 11)
        REQ_NR=30 ;;   #4*30
   02)
        REQ_NR=28 ;;   #4*28
  esac 
  ACT_NR=`ls -l ${sta}/wfe/*cam2.h${ACT_N1}.${WF_YEAR}-${WF_MONTH}-??-?????*nc | grep -v ^d | wc -l`
  echo "Feature detection for " ${WF_YEAR}"-"${WF_MONTH} " for case "${CASE}

  if [ "$ACT_NR" -ne "$REQ_NR" ]; then
    echo "=================  FEATURE DETECTION  ================"
    echo "Insufficiently many CAM output files for "${WF_YEAR}-${WF_MONTH} "in" ${sta}/wfe/
    echo "Only " $ACT_NR "are available instead of " $REQ_NR
    echo "No feature detection for this month."
  fi  
  #===============================
  #=== adapt feature detection setting 
  MMONTH=$(echo ${WF_MONTH} | sed 's/^0*//')
  sed -i s/"yr, month =".*/"yr, month = "${WF_YEAR}", "${MMONTH}/g $CCASETOOLS/${NAD_SCRIPT}  
  #=== detect features
  echo "determine aggregated weather features for " ${WF_YEAR}-${WF_MONTH}
  echo "   using  " $CCASETOOLS/${NAD_SCRIPT} " | " $ACT_NR " files from month "${WF_MONTH}
  #==
  #/cluster/software/Python/3.6.4-intel-2018a/bin/python $CCASETOOLS/${NAD_SCRIPT}
  python $CCASETOOLS/${NAD_SCRIPT}
  #=== del/mv 6hourly output used for dynlib from month-2 
  dnamepprec=$(date -d "$dnameact - 2 months" +"%Y-%m-%d")
  WF_PPYEAR=${dnamepprec:0:4}
  WF_PPMONTH=${dnamepprec:5:2}
  PPRE_NR=`ls -l ${sta}/wfe/*cam2.h${ACT_N1}.${WF_PPYEAR}-${WF_PPMONTH}-??-?????*nc | grep -v ^d | wc -l`
  if [ "$PPRE_NR" -gt 0 ]; then
    if (( ${RETAIN_HIFREQ} != 1 )); then
      rm ${sta}/wfe/*cam2.h${ACT_N1}.${WF_PPYEAR}-${WF_PPMONTH}-??-?????*nc 
    else
      mv ${sta}/wfe/*cam2.h${ACT_N1}.${WF_PPYEAR}-${WF_PPMONTH}-??-?????*nc ${sta}/wfe/dump/.
    fi 
  fi
done
end=$(date +'%s')
let time_dynlib="end-start"

#== mv last 6hr temporary file back to run folder (otherwise crash when restart)
LATEST=`ls -rt ${sta}/wfe/*cam2.h${ACT_N1}*nc | tail -1`
mv ${LATEST} .
#=== archive features
echo " archive feature files "
ls *dynlib.mon_feats*nc
mv *dynlib.mon_feats*nc ${sta}/wfe/.

#=== go back to original path 
cd ${ACTUAL_PATH1}


echo "======================================================================================================="
echo "Elapsed time for feature detection:                                       "${time_dynlib}"sec"
echo "======================================================================================================="
