#############################################################################
#
# INITIAL DEVELOPMENT: Madlen Kimmritz, Sep 2019
#
# PURPOSE: specification of online detections of weather features and storm 
#          track diagnostics for a simulation of NorESM prediction / 
#          NorCPM analysis
#
# for further information see also README_MTK.txt and documentation_MTK.pdf
#
#############################################################################

# A) BASIC SETTING
DL_ROOT="/cluster/home/$USER/NorCPM/dynlib/"

WF_IS_PREDICTION=0
        # set to 1 if weather features determined for prediction run
        # set to 0 if weather features determined for analysis run 

DYN_OUT=('JETS' 'RWB' 'FRONTLINES' 'FRONTVOLUMES' 'EDDYVAR' 'CYCLTRACK' 'BLOCK')
        #list of diagnostics provided by dynlib, do no use commata between entries
        #NO SPACE before and after "=", NO COMMATE between entries
        #available diagnostics are: 
        #     'JETS' 'RWB' 'FRONTLINES' 'FRONTVOLUMES' 'EDDYVAR' 'CYCLTRACK' 'BLOCK'
        #

DYN_ADD_OUT=('T:I') 
        #list of 6-hrly output of CAM variables to be archived
        #NO SPACE before and after "=", NO COMMATE between entries
        #snapshots: add ':I' to var name  (e.g. 'T:I'); 6hrl mean: bare var name (e.g. 'T')
        #see also: http://www.cesm.ucar.edu/models/cesm1.0/cam/docs/ug5_0/hist_flds_fv_cam4.html

RETAIN_HIFREQ=0
        # set to 1 if 6-hourly TEMPORARY files to determine monthly features shall be retained
        # NOTE: elements listed in DYN_ADD_OUT are always archived independent of RETAIN_HIFREQ.

# B) AUTOMATED INFORMATION
#    NOTE: You DO NOT need to MODIFY the following lines. They will be adapted automatically due 
#          to the atmospheric (cam2) output setting of NorESM/NorCPM.
DYN_H1=1
DYN_H2=2


