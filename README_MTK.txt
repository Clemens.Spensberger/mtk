#############################################################################
#
#   README: MIND the KAP
# 
#   INITIAL DEVELOPMENT: Clemens Spensberger (dynlib and noresm_autodetect), 
#                        Madlen Kimmritz (NorESM/NorCPM related), Aug-Dec 2019
#
#   The manuscript documentation_MTK.pdf (in this folder) provides more 
#       detailed information.
#############################################################################

 PURPOSE: activate the use of dynlib for NorESM predictions and NorCPM reanalyses 
          to derive weather features and storm track diagnostics online during
          model run

#############################################################################

 REQUIREMENTS

 LIBRARIES
 On Fram DYNLIB requires the following modules
       - Python/3.6.4-intel-2018a
       - ScaLAPACK/2.0.2-gompi-2018a-OpenBLAS-0.2.20
       - netcdf4-python/1.4.0-intel-2018a-Python-3.6.4

 DYNLIB
 to derive DYNLIB, 
  cd NorCPM-root folder
  git clone https://git.app.uib.no/Clemens.Spensberger/dynlib.git 
  cd dynlib
  ./compile

 SETTING PECULARITIES FOR PREDICTION
   STOP_OPTION=nmonths or nyears # units for run length specification STOP_N 
   For ANALYSIS case: STOP_OPTION has to be nmonths.

 TO ENABLE AND RUN PREDICTION / ANALYSIS WITH ENABLED FEATURE DETECTION
  - modify setting_MTK.sh and run prediction/analysis_template.sh
  - NOTE, that no issues related to NorCPM/NorESM have been treated.
    For questions related to NorCPM/NorESM contact personnell on NorCPM-wiki-page
    
#############################################################################
 FEATURE DETECTION 
 - are called in the beginning of ST_ARCHIVE.sh for each member and case individually for 
   STOP_N STOP_OPTION (for analysis with STOP_N=1 and STOP_OPTION=nmonth features are 
                       detected each month)
#############################################################################

 AVAILABLE DIAGNOSTICS via DYNLIB

 ACRONYM         DESCRIPTION
 =======         ===========
 JETS           Jet axes (Spensberger et al., 2017)
 RWB            Rossby wave breaking (Rivière, 2007)
 FRONTLINES     Front lines (Jenkner et al., 2010, & Schemm et al., 2015)
 FRONTVOLUMES   Frontal volumes (Spensberger & Sprenger, 2018)
 EDDYVAR        Eddy covariances u'v', v'T', u'u', v'v' and z'z' 
 CYCTRACK       Cyclone tracks (e.g., after Murray and Simmonds, 1991)
 BLOCK          Atmospheric blocks (Masato et al., 2014)
 BLOCK          Atmospheric blocks after Masato et al. (2014)



