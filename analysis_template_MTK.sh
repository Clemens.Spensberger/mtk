#!/bin/sh -evx 
#############################################################################
#
# INITIAL DEVELOPMENT: Madlen Kimmritz, Dec 2019
#
# This script is a mimic of prediction_template_MTK.in 
# for the use within the NorCPM analysis framework
#
# PURPOSE: a) Create case with enabled use of dynlib for NorCPM reanalyses
#             to derive weather features and storm track diagnostics (DYNLIB)
#             of the analysis
#          b) Submit script to run reanalysis
#
# HOW TO USE:
# 1) adapt setting_MTK.sh of MTK/  (in particular: set WF_IS_PREDICTION = 0)
# 2) adapt default_setting.sh of analysis/setting/
# 3) run the script 
#
#  see also README_MTK.txt and documentation_MTK.pdf 
#
#  NOTE: it might be wise to run the different steps separately
#        as the restarts might not be in the format requested by create_ensemble.sh
#
#  NOTE: Take care, that the setting files in the different NorCPM scripts agree 
#        with the ones listed here.
#
############################################################################

SCRIPTPATH=`readlink -f $0`
SCRIPTDIR=`dirname $SCRIPTPATH`
INFILE_SHORT=default_setting_V0_temp.sh
INFILE=$SCRIPTDIR/../analysis/setting/${INFILE_SHORT} #former .in
WFFILE=$SCRIPTDIR/../MTK/setting_MTK.sh

#===
cd $SCRIPTDIR/../analysis
#=====================================
# STEP 0: (NorCPM standard) 
#         delete all related to INFILE setting to ensure start from a clean set up
echo CLEANUP ENSEMBLE
./cleanup_ensemble.sh $INFILE_SHORT

#=====================================
# step 1: (MIND the KAP specifics)
#         insert line in create_ensemble.sh to enable use of MTK
if [ 0 == 0 ]; then
echo ADAPT CREATE ENSEMBLE SCRIPT TO ENABLE USE OF MTK 
cp create_ensemble.sh create_ensemble_MTK.sh
chmod 755 create_ensemble_MTK.sh

#== insert call of modify_setting_MTK.sh directly after template case has been created
insert1='#== madlen.kimmritz@awi.de: Additional handling of weather feature detection.'
insert2=$SCRIPTDIR/../MTK/modify_setting_MTK.sh
insert2a=${INFILE//\//\\/}  #replace every "/" by "\/"
insert2b=${WFFILE//\//\\/}   
insert3='#== end: Additional handling of weather feature detection.'
Bin2=${insert2//\//\\/} 
linem="`grep "Prepare the rest of the members" create_ensemble_MTK.sh`"
sed -i "s/${linem}/${insert1}\n${linem}/" create_ensemble_MTK.sh   
linem="`grep "Prepare the rest of the members" create_ensemble_MTK.sh`"
sed -i "s/${linem}/${Bin2} ${insert2a} ${insert2b}\n${linem}/" create_ensemble_MTK.sh   
linem="`grep "Prepare the rest of the members" create_ensemble_MTK.sh`"
sed -i "s/${linem}/${insert3}\n${linem}/" create_ensemble_MTK.sh   
fi

#=====================================
# STEP 2:  (NorCPM standard)
#          submit modified script to create ensemble 
#echo CREATE ENSEMBLE
./create_ensemble_MTK.sh $INFILE_SHORT

#=====================================
# step 3: (MIND the KAP specifics)
#echo ADAPT SUBMISSION SCRIPT FOR DYNLIB APPLICATION
$SCRIPTDIR/../MTK/modify_submit_MTK.sh $INFILE $WFFILE


#=====================================
# STEP 4: (NorCPM standard
#         basic checks and submit script
#         NOTE, that the $INFILE is hard encoded in submit_reanalysis.sh. 
#               It should agree with the name given above.
#== on fram
#      sbatch submit_reanalysis.sh
#== on hexagon
#      qsub submit_reanalysis.sh



